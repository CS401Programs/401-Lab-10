import java.io.*;
import java.util.*;

public class Lab10 {

	private static TreeSet<String> dictionary = new TreeSet<String>();
	private static TreeSet<String> jumbles = new TreeSet<String>();
	private static TreeMap<String,String> answerKey = new TreeMap<String,String>();
	
	public static void main(String[] args) throws IOException {
		
		// Check to see if the user gave the proper arguments
		if (args.length < 1 )
		{
			System.out.println("\nusage: C:\\> java Lab10 dictionary.txt jumbles.txt\n\n");
			System.exit(0);
		}

		// The steps to solve the word jumble
		// are broken up into 3 method calls
		buildList(args);
		buildKey();
	}
	
	private static void buildList(String args[]) throws IOException {
		
		BufferedReader dictReader = new BufferedReader(new FileReader(args[0]));
		BufferedReader jumblesReader = new BufferedReader(new FileReader(args[1]));
	
		while (dictReader.ready())
			dictionary.add(dictReader.readLine());
		dictReader.close();
	
		while (jumblesReader.ready())
			jumbles.add(jumblesReader.readLine());
		jumblesReader.close();
	
	}
	
	private static void buildKey() {
		
		for (String dWord : dictionary) {
			String dCanon = toCanon(dWord);
			
			if (!answerKey.containsKey(dCanon))
				answerKey.put(dCanon, dWord);
			else
				answerKey.put(dCanon, answerKey.get(dCanon) + " " + dWord);
		}
		
		for (String jWord : jumbles) {
			String jCanon = toCanon(jWord);
			
			System.out.print(jWord + " ");
			if (answerKey.containsKey(jCanon))
				System.out.println(answerKey.get(jCanon));
			else
				System.out.println();
		}
	}
		
	private static String toCanon(String s) {
		
		char[] letters = s.toCharArray();
		Arrays.sort(letters);
		return new String(letters);
	}
}
	